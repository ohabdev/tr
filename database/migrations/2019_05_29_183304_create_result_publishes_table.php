<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultPublishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_publishes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('branches_id');
            $table->string('branch_code');
            $table->string('student_name');
            $table->string('father_name');
            $table->string('mother_name');
            $table->date('date_of_birth');
            $table->string('address');
            $table->string('mobile_number');
            $table->unsignedBigInteger('courses_id');
            $table->string('course_code');
            $table->string('registration_number');
            $table->string('serial_number');
            $table->unsignedBigInteger('batches_id');
            $table->string('semester');
            $table->string('total_marks');
            $table->string('get_marks');
            $table->string('cgpa');
            $table->string('grade');
            $table->string('image');
            $table->string('status');
            $table->timestamps();
            $table->foreign('branches_id')->references('id')->on('branch_registrations');
            $table->foreign('courses_id')->references('id')->on('courses');
            $table->foreign('batches_id')->references('id')->on('batches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_publishes');
    }
}
