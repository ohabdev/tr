<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_registrations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('regional_centre');
            $table->string('branch_code');
            $table->string('chairman_name');
            $table->string('address');
            $table->string('mobile_number');
            $table->string('password');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_registrations');
    }
}
