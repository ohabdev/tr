$(document).ready(function(){
	var baseURL = "http://localhost/tr";
  	$("#result").on("submit", function(){
		var url = baseURL;
		var type = $(this).attr('method');
		var formData = $(this).serialize();
		
		$.ajax({
			url: url,
	       	type: type,
	       	data: formData,
	       	cache: false,
	      	contentType: false,
	       	processData: false,
			success: function(data){
				console.log(data);
				$("#result_id").text(data[0].id);
			}
		});
	});
});