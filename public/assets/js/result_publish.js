$(document).ready(function(){

	var baseURL = "http://localhost/tr";

	$("#branches_id").change(function(){
		var id = $(this).val();
		$.ajax({
			url: baseURL + '/branches_id',
			method: "get",
			data:{id:id},
			success: function(data)
			{
				if (data != '')
				{
					$("#branch_code_view").val(data[0].branch_code);
				}else{
					$("#branch_code_view").val("Code not found");
				}
			}
		});
	});

	$("#courses_id").change(function(){
		var id = $(this).val();
		$.ajax({
			url: baseURL + '/courses_id',
			method: "get",
			data:{id:id},
			success: function(data)
			{
				console.log(data);
				if (data != '')
				{
					$("#courses_code_view").val(data[0].code);
					$(".courses_code_view").text(data[0].code);
					$(".courses_name_view").text(data[0].name);
					$(".courses_marks_view").val(700);
				}else{
					$("#courses_code_view").val("Code not found");
					$("#courses_name_view").val("Code not found");
					$("#courses_marks_view").val("700");
				}
			}
		});
	});

	$("#get_marks").keyup(function(){
		$("#get_marks").each(function() {
		  var marks = $( this ).val();

		  if (marks >= 701){
		  	$(".Grade").val("Rasult Not Found");
		  }else if(marks >= 600){
		  	$(".Grade").val("A+");
		  }else if(marks >= 500){
		  	$(".Grade").val("A");
		  }else if(marks > 599){
		  	$(".Grade").val("A");
		  }else if (marks < 499 ){
		  	$(".Grade").val("A-");
		  }else if (marks < 399 ){
		  	$(".Grade").val("Not Found");
		  }else{
		  	$(".Grade").val("Rasult Not Found");
		  }
		  	
		});
	});

	LoadAllResults();

	function LoadAllResults(){
		$.ajax({
			url: baseURL + '/all_result_load',
			method: "GET",
			success: function(data){
				var row_data = '';
				var sl = 1;
				var i = 0;
				for(i=0; i<data.length; i++)
				{
					row_data += '<tr>'; 
					row_data += '<td>'+ sl++ +'</td>';
					row_data += '<td>'+data[i].branch_name+'</td>';
					row_data += '<td>'+data[i].student_name+'</td>';
					row_data += '<td>'+data[i].course_name+'</td>';
					row_data += '<td>'+data[i].batch_name+'</td>';
					row_data += '<td>'+data[i].registration_number+'</td>';
					row_data += '<td>'+data[i].serial_number+'</td>';
					row_data += '<td>'+data[i].mobile_number+'</td>';
					if (data[i].status == 1) {
						row_data += '<td><span class="btn btn-sm btn-success" style="font-weight: bolder;"> Published </span></td>';
					}else{
						row_data += '<td><span class="btn btn-sm btn-warning" style="color:green; font-weight: bolder;"> Pending </span></td>';
						row_data += '<td><a href="#" class="btn btn-sm btn-info" data-toggle="modal" data-target="#statusModal{{ $regional->id}}" class="mr-2" title="Approve!"> Approve </a></td>';
					}
					row_data += '<td>';
					row_data += '<div class="dropdown">';
					row_data += '<a class="btn btn-outline-primary dropdown-toggle" role="button" data-toggle="dropdown">';
					row_data += '<i class="fa fa-ellipsis-h"></i></a>';
					row_data += '<div class="dropdown-menu dropdown-menu-right">';
					row_data += '<a class="dropdown-item" href="employee/view" id="employee_view" data-id="'+data[i].id+'" data-toggle="modal" data-target="#employee_view_modal"><i class="fa fa-eye"></i> View</a>';
					row_data += '<a class="dropdown-item" href="employee/edit" id="employee_edit" data-id="'+data[i].id+'" data-toggle="modal" data-target="#employee_edit_modal"><i class="fa fa-pencil"></i> Edit</a>';
					row_data += '<a class="dropdown-item" href="employee/delete_view" id="employee_delete" data-id="'+data[i].id+'" data-toggle="modal" data-target="#employee_delete_modal"><i class="fa fa-trash"></i>Delete</a>';
					row_data += '</div></div>';
					row_data += '</td>';
					row_data += '</tr>';
				}
				$("#show_all_results").html(row_data);
			}	
		});
	}



});