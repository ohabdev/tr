<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/admin', 'HomeController@index')->name('admin');

//Course
Route::get('/courses', 'CourseController@index')->name('courses');
Route::post('/course-create', 'CourseController@create')->name('course-create');
Route::post('/course-status', 'CourseController@changeStatus')->name('course-status');
Route::post('/course-update', 'CourseController@update')->name('course-update');
Route::post('/course-delete', 'CourseController@destroy')->name('course-delete');

//Batches
Route::get('/batches', 'BatchController@index')->name('batches');
Route::post('/batch-create', 'BatchController@create')->name('batch-create');
Route::post('/batch-status', 'BatchController@changeStatus')->name('batch-status');
Route::post('/batch-update', 'BatchController@update')->name('batch-update');
Route::post('/batch-delete', 'BatchController@destroy')->name('batch-delete');

//=======================Branch Registration=========================//

Route::get('/branch_registration', 'BranchRegistrationController@index')->name('branch_registration');
Route::get('/branch_view', 'BranchRegistrationController@branch_view')->name('branch_view');
Route::post('/branch_store', 'BranchRegistrationController@store')->name('branch_store');
Route::post('/regional-status/{id}', 'BranchRegistrationController@approve')->name('regional-status');
Route::post('/regional-delete/{id}', 'BranchRegistrationController@destroy')->name('regional-delete');

//=======================Result Publish========================//

Route::get('/result_publish', 'ResultPublishController@index')->name('resultPublish');
Route::get('/branches_id', 'ResultPublishController@branches_id')->name('branches_id');
Route::get('/courses_id', 'ResultPublishController@courses_id')->name('courses_id');
Route::post('/result_store', 'ResultPublishController@result_store')->name('result_store');
Route::get('/all_result', 'ResultPublishController@all_result')->name('all_result');
Route::get('/all_result_load', 'ResultPublishController@all_result_load')->name('all_result_load');

//=======================Get Result========================//

Route::get('/get_result', 'GetResultController@index')->name('get_result');
Route::post('/result_info', 'GetResultController@result_info')->name('result_info');
Route::post('/result_view', 'GetResultController@result_view')->name('result_view');