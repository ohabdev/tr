<!DOCTYPE html>
<html lang="en">
<head>
	<!-- META ============================================= -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<meta name="robots" content="" />
	<!-- DESCRIPTION -->
	<meta property="og:image" content="" />
	<meta name="format-detection" content="telephone=no">
	<!-- FAVICONS ICON ============================================= -->
	<link rel="icon" href="{{ asset('public/assets_front')}}/images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/assets_front')}}/images/favicon.png" />
	<!-- PAGE TITLE HERE ============================================= -->
	<title> Branch Registration </title>
	<!-- MOBILE SPECIFIC ============================================= -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--[if lt IE 9]>
	<script src="{{ asset('public/assets_front')}}/js/html5shiv.min.js"></script>
	<script src="{{ asset('public/assets_front')}}/js/respond.min.js"></script>
	<![endif]-->
	<!-- All PLUGINS CSS ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets_front')}}/css/assets.css">
	<!-- TYPOGRAPHY ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets_front')}}/css/typography.css">
	<!-- SHORTCODES ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets_front')}}/css/shortcodes/shortcodes.css">
	<!-- STYLESHEETS ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets_front')}}/css/style.css">
	<link class="skin" rel="stylesheet" type="text/css" href="{{ asset('public/assets_front')}}/css/color/color-1.css">
</head>
<body id="bg">
<div class="page-wraper">
	<div id="loading-icon-bx"></div>
	<div class="account-form">
		<div class="account-head" style="background-image:url(public/assets_front/images/background/bg2.jpg);">
			<a href="{{ url('/branch_registration')}}"><img src="{{ asset('public/assets_front')}}/images/logo-white-2.png" alt=""></a>
		</div>
		<div class="account-form-inner">
			<div class="account-container">
				@if(Session::has('message'))
	                <div class="notification"><div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ Session::get('message') }}</div></div>
	            @endif
				<div class="heading-bx left">
					<h4 class="title-head"> Regional Centre Regestation </h4>
				</div>	
				<form class="contact-bx" action="{{ url('/branch_store') }}" method="POST">
					@csrf
					<div class="row placeani">
						<div class="col-lg-12">
							<div class="form-group">
								<div class="input-group">
									<label>Name Of Regional Centre </label>
									<input name="regional_centre" type="text" required="" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<div class="input-group">
									<label> Branch Code</label>
									<input name="branch_code" type="text" required="" class="form-control">
								</div>
							</div>
						</div>

						<div class="col-lg-12">
							<div class="form-group">
								<div class="input-group"> 
									<label> Chairman Name </label>
									<input name="chairman_name" type="text" class="form-control" required="">
								</div>
							</div>
						</div>

						<div class="col-lg-12">
							<div class="form-group">
								<div class="input-group"> 
									<label> Address  </label>
									<input name="address" type="text" class="form-control" required="">
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<div class="input-group"> 
									<label>Mobile Number </label>
									<input name="mobile_number" type="number" class="form-control" required="">
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<div class="input-group"> 
									<label> Password </label>
									<input name="password" type="password" class="form-control" required="">
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<div class="input-group"> 
									<label> Confirm Password </label>
									<input name="confirm_password" type="password" class="form-control" required="">
								</div>
							</div>
						</div>
						<div class="col-lg-12 m-b30">
							<button type="submit" value="Submit" class="btn button-md">Registation</button>
							<a href="./" class="btn button-md">Cancel</a>
						</div>		
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- External JavaScripts -->
<script src="{{ asset('public/assets_front')}}/js/jquery.min.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/bootstrap/js/popper.min.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/magnific-popup/magnific-popup.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/counter/waypoints-min.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/counter/counterup.min.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/imagesloaded/imagesloaded.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/masonry/masonry.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/masonry/filter.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/owl-carousel/owl.carousel.js"></script>
<script src="{{ asset('public/assets_front')}}/js/functions.js"></script>
<script src="{{ asset('public/assets_front')}}/js/contact.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/switcher/switcher.js"></script>
</body>
</html>
