<!DOCTYPE html>
<html lang="en">
<head>
	<!-- META ============================================= -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<meta name="robots" content="" />
	<!-- DESCRIPTION -->
	<meta property="og:image" content="" />
	<meta name="format-detection" content="telephone=no">
	<!-- FAVICONS ICON ============================================= -->
	<link rel="icon" href="{{ asset('public/assets_front')}}/images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/assets_front')}}/images/favicon.png" />
	<!-- PAGE TITLE HERE ============================================= -->
	<title> Get Result </title>
	<!-- MOBILE SPECIFIC ============================================= -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--[if lt IE 9]>
	<script src="{{ asset('public/assets_front')}}/js/html5shiv.min.js"></script>
	<script src="{{ asset('public/assets_front')}}/js/respond.min.js"></script>
	<![endif]-->
	<!-- All PLUGINS CSS ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets_front')}}/css/assets.css">
	<!-- TYPOGRAPHY ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets_front')}}/css/typography.css">
	<!-- SHORTCODES ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets_front')}}/css/shortcodes/shortcodes.css">
	<!-- STYLESHEETS ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets_front')}}/css/style.css">
	<link class="skin" rel="stylesheet" type="text/css" href="{{ asset('public/assets_front')}}/css/color/color-1.css">
	<style type="text/css">
		.my-form-control{
			display: block;
		    width: 100%;
		    height: 34px;
		    padding: 6px 12px;
		    font-size: 14px;
		    line-height: 1.42857143;
		    color: #555;
		    background-color: #fff;
		    background-image: none;
		    border: 1px solid #ccc;
		    border-radius: 4px;
		}
	</style>
</head>
<body>
	<br>
	<div class="container">
		<div class="card">
		  	<div class="card-header"><img src="{{ asset('public')}}/banner.jpg" width="100%" height="200"></div>
		  	<div class="card-body">
		  		<form action="{{ url('./result_info')}}" method="POST" target="_blank" id="result">
		  			@csrf
		  			<div class="card">
				  	<div class="card-header"><h4 class="text-center">Input your informtion and get result.</h4></div>
				  	<div class="card-body">
				    	<div class="row">
				    		<div class="col-md-2"></div>
				    		<div class="col-md-4">
				    			<h5> Course Name</h5>
				    		</div>
				    		<div class="col-md-1">:</div>
				    		<div class="col-md-4">
				    			<select name="courses_id" class="my-form-control" required>
				    				<option value=""> Select Course </option>
				    				@foreach ($courses as $key => $course)
				    					<option value="{{ $course->id}}" > {{ $course->name}} </option>
				    				@endforeach
				    			</select>
				    		</div>
				    		<div class="col-md-1"></div>
				    	</div>
				    	<hr>
				    	<div class="row">
				    		<div class="col-md-2"></div>
				    		<div class="col-md-4">
				    			<h5> Serial Number </h5>
				    		</div>
				    		<div class="col-md-1">:</div>
				    		<hr>
				    		<div class="col-md-4">
				    			<input type="text" name="serial_number" class="my-form-control" required>
				    		</div>
				    		<div class="col-md-1"></div>
				    	</div>
				    	<hr>
				    	<div class="row">
				    		<div class="col-md-2"></div>
				    		<div class="col-md-4">
				    			<h5> Registration Number </h5>
				    		</div>
				    		<div class="col-md-1">:</div>
				    		<hr>
				    		<div class="col-md-4">
				    			<input type="text" name="registration_number" class="my-form-control" required>
				    		</div>
				    		<div class="col-md-1"></div>
				    	</div>
				    	<hr>
				    	<div class="row">
				    		<div class="col-md-5"></div>
				    		<div class="col-md-2">
				    			<button class="btn btn-lg btn-danger" type="reset" style=" font-weight: bolder;"> Reset </button>
				    		</div>
				    		<div class="col-md-2">
				    			<button type="submit"  id="get_result" class="btn btn-lg btn-success" style=" font-weight: bolder;"> Get Result </button>
					    	</div>
				    		<div class="col-md-3"></div>
				    	</div>
					  	</div>
					</div>
				</form>
		  	</div>
		  	<div class="card-header"><img src="{{ asset('public')}}/footer.jpg" width="100%"></div>
		</div>
		<br><br>
	</div>
<!-- External JavaScripts -->
<script src="{{ asset('public/assets_front')}}/js/jquery.min.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/bootstrap/js/popper.min.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/bootstrap/js/bootstrap.min.js"></script>
<!-- <script src="{{ asset('public/assets_front')}}/vendors/bootstrap-select/bootstrap-select.min.js"></script> -->
<script src="{{ asset('public/assets_front')}}/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/magnific-popup/magnific-popup.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/counter/waypoints-min.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/counter/counterup.min.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/imagesloaded/imagesloaded.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/masonry/masonry.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/masonry/filter.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/owl-carousel/owl.carousel.js"></script>
<script src="{{ asset('public/assets_front')}}/js/functions.js"></script>
<script src="{{ asset('public/assets_front')}}/js/result.js"></script>
<!-- <script src="{{ asset('public/assets_front')}}/vendors/switcher/switcher.js"></script> -->
</body>
</html>
