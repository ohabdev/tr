<!DOCTYPE html>
<html lang="en">
<head>
	<!-- META ============================================= -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<meta name="robots" content="" />
	<!-- DESCRIPTION -->
	<meta property="og:image" content="" />
	<meta name="format-detection" content="telephone=no">
	<!-- FAVICONS ICON ============================================= -->
	<link rel="icon" href="{{ asset('public/assets_front')}}/images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/assets_front')}}/images/favicon.png" />
	<!-- PAGE TITLE HERE ============================================= -->
	<title> Result Information </title>
	<!-- MOBILE SPECIFIC ============================================= -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--[if lt IE 9]>
	<script src="{{ asset('public/assets_front')}}/js/html5shiv.min.js"></script>
	<script src="{{ asset('public/assets_front')}}/js/respond.min.js"></script>
	<![endif]-->
	<!-- All PLUGINS CSS ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets_front')}}/css/assets.css">
	<!-- TYPOGRAPHY ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets_front')}}/css/typography.css">
	<!-- SHORTCODES ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets_front')}}/css/shortcodes/shortcodes.css">
	<!-- STYLESHEETS ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets_front')}}/css/style.css">
	<link class="skin" rel="stylesheet" type="text/css" href="{{ asset('public/assets_front')}}/css/color/color-1.css">
	<style type="text/css">
		.my-form-control{
			display: block;
		    width: 100%;
		    height: 34px;
		    padding: 6px 12px;
		    font-size: 14px;
		    line-height: 1.42857143;
		    color: #555;
		    background-color: #fff;
		    background-image: none;
		    border: 1px solid #ccc;
		    border-radius: 4px;
		}
	</style>
</head>
<body>
	<br>
	<div class="container">
		<div class="card">
		  	<div class="card-header"><img src="{{ asset('public')}}/banner.jpg" width="100%" height="150"></div>
		  	<div class="card-body">
	  			<div class="card bg-light">
				  	<div class="card-header "><h4 class="text-center ">Result Details</h4></div>
				  	<div class="card-header "></div>
				  	<div class="device-width">
				  		<div class="card-header">
				  			<div class="row">
				  				<div class="col-md-8" style="border-right:1px solid black; ">
				  					<div class="row">
				  						<div class="col-md-5">
				  							<h4>Registraion Number</h4>
				  						</div>
				  						<div class="col-md-1">:</div>
				  						<div class="col-md-6">
				  							<h4>14787965658987</h4>
				  						</div>
				  					</div>
				  					<div class="row">
				  						<div class="col-md-5">
				  							<h4> Serial  Number</h4>
				  						</div>
				  						<div class="col-md-1">:</div>
				  						<div class="col-md-6">
				  							<h4>14787965658987</h4>
				  						</div>
				  					</div>
				  					<div class="row">
				  						<div class="col-md-5">
				  						<h4>	Student Name</h4>
				  						</div>
				  						<div class="col-md-1">:</div>
				  						<div class="col-md-6">
				  							<h4>14787965658987</h4>
				  						</div>
				  					</div>
				  				</div>
				  				<div class="col-md-4">
				  					<div class="card">
				  						<!-- <img src="public/banner.jpg" class="img-thumbnail"> -->
				  					</div>
				  				</div>
				  			</div>
				  		</div>
				  	</div>
				  	<div class="card-body">
				    	<div class="row">
				    		<div class="col-md-2"></div>
				    		<div class="col-md-8">
				    			<table class="table table-hover">
								  	<tbody>
									    <tr>
									      <th scope="row">1</th>
										    <td>Father's Name</td>
										    <td>Md.Anarul Islam</td>
									    </tr>
										 <tr>
									      <th scope="row">2</th>
										    <td>Mother's Name</td>
										    <td>Mst.Minara Begum</td>
									    </tr>
									    <tr>
									      <th scope="row">3</th>
										    <td>Course Name</td>
										    <td>Diploma In Computer Scince "ICT"</td>
									    </tr><tr>
									      <th scope="row">4</th>
										    <td>Batch</td>
										    <td>01 Januwary 2019 TO 30 Jun 2019 </td>
									    </tr><tr>
									      <th scope="row">5</th>
										    <td>Total Marks</td>
										    <td>700</td>
									    </tr><tr>
									      <th scope="row">6</th>
										    <td>Obtained Marks</td>
										    <td>550</td>
									    </tr><tr>
									      <th scope="row">7</th>
										    <td>CGPA</td>
										    <td>3.75</td>
									    </tr><tr>
									      <th scope="row">8</th>
										    <td>GRADE</td>
										    <td>A</td>
									    </tr>
								  	</tbody>
								</table>
				    		</div>
				    		<div class="col-md-2"></div>
				    	</div>
				    	<div class="card">
				    		<div class="card-body">
				    			<div class="row">
				    				<div class="col-md-3" style="border-right:1px solid black; padding-top: 50px;">
				    					<h4 class="text-center">Branch Details</h4>
				    				</div>
				    				<div class="col-md-1"></div>
				    				<div class="col-md-7">
				    					<div class="row">
						    				<div class="col-md-3">
						    					<H5> Branch Name</H5>
						    				</div>
						    				<div class="col-md-1">:</div>
						    				<div class="col-md-8">
						    					<H5> 45455444</H5>
						    				</div>
						    			</div>
				    					<div class="row">
						    				<div class="col-md-3">
						    					<H5> Branch Code</H5>
						    				</div>
						    				<div class="col-md-1">:</div>
						    				<div class="col-md-8">
						    					<H5> 45455444dgdghdegfdsd</H5>
						    				</div>
						    			</div>
				    					<div class="row">
						    				<div class="col-md-3">
						    					<H5> Branch Address</H5>
						    				</div>
						    				<div class="col-md-1">:</div>
						    				<div class="col-md-8">
						    					<H5> 45455444dfsgdsgdf</H5>
						    				</div>
						    			</div>
				    				</div>
				    			</div>
				    		</div>
				    	</div>
					</div>
				</div>
		  	</div>
		  	<div class="card-header"><img src="{{ asset('public')}}/footer.jpg" width="100%"></div>
		</div>
		<br><br>
	</div>
<!-- External JavaScripts -->
<script src="{{ asset('public/assets_front')}}/js/jquery.min.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/bootstrap/js/popper.min.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/bootstrap/js/bootstrap.min.js"></script>
<!-- <script src="{{ asset('public/assets_front')}}/vendors/bootstrap-select/bootstrap-select.min.js"></script> -->
<script src="{{ asset('public/assets_front')}}/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/magnific-popup/magnific-popup.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/counter/waypoints-min.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/counter/counterup.min.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/imagesloaded/imagesloaded.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/masonry/masonry.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/masonry/filter.js"></script>
<script src="{{ asset('public/assets_front')}}/vendors/owl-carousel/owl.carousel.js"></script>
<script src="{{ asset('public/assets_front')}}/js/functions.js"></script>
<script src="{{ asset('public/assets_front')}}/js/contact.js"></script>
</body>
</html>
