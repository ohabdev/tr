<!DOCTYPE html>
<html lang="en">
<head>
	<!-- META ============================================= -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<meta name="robots" content="" />
	<!-- DESCRIPTION -->
	<meta property="og:image" content="" />
	<meta name="format-detection" content="telephone=no">
	<!-- FAVICONS ICON ============================================= -->
	<link rel="icon" href="{{ asset('public/assets_front')}}/images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/assets_front')}}/images/favicon.png" />
	<!-- PAGE TITLE HERE ============================================= -->
	<title> Result Information </title>
	<!-- MOBILE SPECIFIC ============================================= -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--[if lt IE 9]>
	<script src="{{ asset('public/assets_front')}}/js/html5shiv.min.js"></script>
	<script src="{{ asset('public/assets_front')}}/js/respond.min.js"></script>
	<![endif]-->
	<!-- All PLUGINS CSS ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets_front')}}/vendors/bootstrap/css/bootstrap.min.css">
	<style type="text/css">
		.my-form-control{
			display: block;
		    width: 100%;
		    height: 34px;
		    padding: 6px 12px;
		    font-size: 14px;
		    line-height: 1.42857143;
		    color: #555;
		    background-color: #fff;
		    background-image: none;
		    border: 1px solid #ccc;
		    border-radius: 4px;
		}
	</style>
</head>
<body>
	<br>
	<?php if(!empty($results)){ ?>
		@foreach($results as $result)
			<div class="container">
				<div class="card">
				  	<div class="card-header"><img src="{{ asset('public')}}/banner.jpg" width="100%" height="150"></div>
				  	<div class="card-body">
			  			<div class="card bg-light">
						  	<div class="card-header "><h4 class="text-center ">Result Details</h4></div>
						  	<div class="device-width">
						  		<div style="border: 1px solid rgba(0,0,0,.125); height: 180px; padding: 15px;">
						  			<div style="width: 69%; float: left; margin-top: 15px;">
						  				<div style="width: 100%; margin-top: 5px;">
						  					<div style="width: 48%; float:left;">
						  						<div style="width: 79%; float: left;">
						  							<h4>Registraion Number</h4>		
						  						</div>
						  						<div style="width: 20%; font-weight: bold; float: right;">:</div>
						  					</div>
						  					<div style="width: 50%; float: right;"><h4 style="float: left;"> {{ $result->registration_number }}</h4></div>
						  				</div>
						  				<div style="width: 100%; margin-top: 5px;">
						  					<div style="width: 48%; float:left;">
						  						<div style="width: 79%; float: left;">
						  							<h4>Serial  Number</h4>		
						  						</div>
						  						<div style="width: 20%; font-weight: bold; float: right;">:</div>
						  					</div>
						  					<div style="width: 50%; float: right;"><h4 style="float: left;"> {{ $result->serial_number }}</h4></div>
						  				</div>
						  				<div style="width: 100%; margin-top: 5px;">
						  					<div style="width: 48%; float:left;">
						  						<div style="width: 79%; float: left;">
						  							<h4>Student Name</h4>		
						  						</div>
						  						<div style="width: 20%; font-weight: bold; float: right;">:</div>
						  					</div>
						  					<div style="width: 50%; float: right;"><h4 style="float: left;"> {{ $result->student_name }}</h4></div>
						  				</div>
						  			</div>
						  			<div style="width: 30%; float: right; border-left:1px solid black;">
						  				<center><img src="storage/app/public/image/{{ $result->image }}" width="150" class="img-thumbnail"></center>
						  			</div>
						  		</div>
						  	</div>
						  	<div class="card-body">
						    	<div class="row">
						    		<div style="width: 60%; margin: 20px auto;">
						    			<table class="table table-hover" style="color:black;">
										  	<tbody>
											    <tr>
											      <th scope="row">1</th>
												    <td>Father's Name</td>
												    <td>{{ $result->father_name }}</td>
											    </tr>
												 <tr>
											      <th scope="row">2</th>
												    <td>Mother's Name</td>
												    <td>{{ $result->mother_name }}</td>
											    </tr>
											    <tr>
											      <th scope="row">3</th>
												    <td>Course Name</td>
												    <td>{{ $result->course_name }}</td>
											    </tr><tr>
											      <th scope="row">4</th>
												    <td>Batch</td>
												    <td>{{ $result->batch_name }} </td>
											    </tr><tr>
											      <th scope="row">5</th>
												    <td>Total Marks</td>
												    <td>{{ $result->total_marks }}</td>
											    </tr><tr>
											      <th scope="row">6</th>
												    <td>Obtained Marks</td>
												    <td>{{ $result->get_marks }}</td>
											    </tr><tr>
											      <th scope="row">7</th>
												    <td>CGPA</td>
												    <td>{{ $result->cgpa }}</td>
											    </tr><tr>
											      <th scope="row">8</th>
												    <td>GRADE</td>
												    <td>{{ $result->grade }}</td>
											    </tr>
										  	</tbody>
										</table>
						    		</div>
						    	</div>
						    	<div class="card">
						    		<div class="card-body">
						    			<div style="width: 100%">
						    				<div style="width: 30%; float: left; padding-top: 30px;">
						    					<h4 class="text-center">Branch Details</h4>
						    				</div>
						    				<div style="width: 69%; float: right; border-left:1px solid black; padding-left: 10px;">
						    					<div style="width: 100%">
						    						<div style="width: 35%; float:left;">
								  						<div style="width: 89%; float: left;">
								  							<h5>Regional Centre</h5>		
								  						</div>
								  						<div style="width: 10%; font-weight: bold; float: right;">:</div>
								  					</div>
								  					<div style="width: 64%; float: right;"><h5 style="text-align: left;"> {{ $result->branch_name }}</h5></div>
						    					</div>
						    					<div style="width: 100%">
						    						<div style="width: 35%; float:left;">
								  						<div style="width: 89%; float: left;">
								  							<h5>Branch Code</h5>		
								  						</div>
								  						<div style="width: 10%; font-weight: bold; float: right;">:</div>
								  					</div>
								  					<div style="width: 64%; float: right;"><h5 style="float: left;"> {{ $result->branch_code }}</h5></div>
						    					</div>
						    					<div style="width: 100%">
						    						<div style="width: 35%; float:left;">
								  						<div style="width: 89%; float: left;">
								  							<h5>Address</h5>		
								  						</div>
								  						<div style="width: 10%; font-weight: bold; float: right;">:</div>
								  					</div>
								  					<div style="width: 64%; float: right;"><h5 style="float: left;"> {{ $result->address }}</h5></div>
						    					</div>
						    				</div>
						    			</div>
						    		</div>
						    	</div>
							</div>
						</div>
				  	</div>
				  	<div class="card-header"><img src="{{ asset('public')}}/footer.jpg" width="100%"></div>
				</div>
				<br><br>
			</div>
		@endforeach
	<?php }else{ ?>
		<div class="notification"><div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Result Not Found!</div></div>
	<?php } ?>

</body>
</html>
