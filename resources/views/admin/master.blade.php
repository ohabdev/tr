<!DOCTYPE html>
<html lang="en">
<head>

	<!-- META ============================================= -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<meta name="robots" content="" />
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- DESCRIPTION -->
	<meta name="description" content="EduChamp : Education HTML Template" />

	<!-- OG -->
	<meta property="og:title" content="EduChamp : Education HTML Template" />
	<meta property="og:description" content="EduChamp : Education HTML Template" />
	<meta property="og:image" content="" />
	<meta name="format-detection" content="telephone=no">

	<!-- FAVICONS ICON ============================================= -->
	<link rel="shortcut icon" type="image/x-icon" href="{{ asset('public') }}/assets/assets/images/favicon.png" />

	<!-- PAGE TITLE HERE ============================================= -->
	<title>Admin :: @yield('title')</title>

	<!-- MOBILE SPECIFIC ============================================= -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!--[if lt IE 9]>
	<script src="{{ asset('public') }}/assets/assets/js/html5shiv.min.js"></script>
	<script src="{{ asset('public') }}/assets/assets/js/respond.min.js"></script>
	<![endif]-->

	<!-- All PLUGINS CSS ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public') }}/assets/assets/css/dashboard.css">
	{{-- <link rel="stylesheet" type="text/css" href="{{ asset('public') }}/assets/vendors/calendar/fullcalendar.css"> --}}

	<!-- TYPOGRAPHY ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public') }}/assets/css/typography.css">

	<!-- SHORTCODES ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public') }}/assets/css/shortcodes/shortcodes.css">

	<!-- STYLESHEETS ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public') }}/assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('public') }}/assets/css/dashboard.css">
	<link class="skin" rel="stylesheet" type="text/css" href="{{ asset('public') }}/assets/css/color/color-1.css">

	<link rel="stylesheet" type="text/css" href="{{ asset('public') }}/assets/vendors/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('public') }}/assets/vendors/fontawesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('public') }}/assets/vendors/themify/themify-icons.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('public') }}/assets/vendors/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('public') }}/assets/vendors/bootstrap-select/bootstrap-select.min.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('public') }}/assets/vendors/magnific-popup/magnific-popup.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('public') }}/assets/vendors/scroll/scrollbar.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('public') }}/assets/vendors/switcher/switcher.css">

	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Raleway:100,200,300,400,500,600,700,800,900|Roboto:100,300,400,500,700,900|Rubik:300,400,500,700,900">
</head>
<body class="ttr-opened-sidebar ttr-pinned-sidebar">

	<!-- header start -->
	@include('admin.inc.headnav')
	<!-- header end -->
	<!-- Left sidebar menu start -->
	@include('admin.inc.leftsidebar')
	<!-- Left sidebar menu end -->
	@yield('mainPart')

	<div class="ttr-overlay"></div>

	<!-- External JavaScripts -->
	<script src="{{ asset('public') }}/assets/assets/js/jquery.min.js"></script>
	<script src="{{ asset('public') }}/assets/assets/vendors/bootstrap/js/popper.min.js"></script>
	<script src="{{ asset('public') }}/assets/assets/vendors/bootstrap/js/bootstrap.min.js"></script>
	<!-- <script src="{{ asset('public') }}/assets/assets/vendors/bootstrap-select/bootstrap-select.min.js"></script> -->
	<script src="{{ asset('public') }}/assets/assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
	<script src="{{ asset('public') }}/assets/assets/vendors/magnific-popup/magnific-popup.js"></script>
	<script src="{{ asset('public') }}/assets/assets/vendors/counter/waypoints-min.js"></script>
	<script src="{{ asset('public') }}/assets/assets/vendors/counter/counterup.min.js"></script>
	<script src="{{ asset('public') }}/assets/assets/vendors/imagesloaded/imagesloaded.js"></script>
	<script src="{{ asset('public') }}/assets/assets/vendors/masonry/masonry.js"></script>
	<script src="{{ asset('public') }}/assets/assets/vendors/masonry/filter.js"></script>
	<script src="{{ asset('public') }}/assets/assets/vendors/owl-carousel/owl.carousel.js"></script>
	<script src="{{ asset('public') }}/assets/assets/vendors/scroll/scrollbar.min.js"></script>
	<script src="{{ asset('public') }}/assets/assets/js/functions.js"></script>
	<script src="{{ asset('public') }}/assets/assets/vendors/chart/chart.min.js"></script>
	<script src="{{ asset('public') }}/assets/assets/js/admin.js"></script>
	<script src="{{ asset('public') }}/assets/assets/vendors/calendar/moment.min.js"></script>
	<script src="{{ asset('public') }}/assets/assets/vendors/calendar/fullcalendar.js"></script>
	<!-- <script src="{{ asset('public') }}/assets/assets/vendors/switcher/switcher.js"></script> -->
	<script src="{{ asset('public') }}/assets/js/result_publish.js"></script>
	<script>
	  $(document).ready(function() {

	    $('#calendar').fullCalendar({
	      header: {
	        left: 'prev,next today',
	        center: 'title',
	        right: 'month,agendaWeek,agendaDay,listWeek'
	      },
	      defaultDate: '2019-03-12',
	      navLinks: true, // can click day/week names to navigate views

	      weekNumbers: true,
	      weekNumbersWithinDays: true,
	      weekNumberCalculation: 'ISO',

	      editable: true,
	      eventLimit: true, // allow "more" link when too many events
	      events: [
	        {
	          title: 'All Day Event',
	          start: '2019-03-01'
	        },
	        {
	          title: 'Long Event',
	          start: '2019-03-07',
	          end: '2019-03-10'
	        },
	        {
	          id: 999,
	          title: 'Repeating Event',
	          start: '2019-03-09T16:00:00'
	        },
	        {
	          id: 999,
	          title: 'Repeating Event',
	          start: '2019-03-16T16:00:00'
	        },
	        {
	          title: 'Conference',
	          start: '2019-03-11',
	          end: '2019-03-13'
	        },
	        {
	          title: 'Meeting',
	          start: '2019-03-12T10:30:00',
	          end: '2019-03-12T12:30:00'
	        },
	        {
	          title: 'Lunch',
	          start: '2019-03-12T12:00:00'
	        },
	        {
	          title: 'Meeting',
	          start: '2019-03-12T14:30:00'
	        },
	        {
	          title: 'Happy Hour',
	          start: '2019-03-12T17:30:00'
	        },
	        {
	          title: 'Dinner',
	          start: '2019-03-12T20:00:00'
	        },
	        {
	          title: 'Birthday Party',
	          start: '2019-03-13T07:00:00'
	        },
	        {
	          title: 'Click for Google',
	          url: 'http://google.com/',
	          start: '2019-03-28'
	        }
	      ]
	    });

	  });

	</script>
	<script type="text/javascript">
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
	</script>
	</body>
</html>
