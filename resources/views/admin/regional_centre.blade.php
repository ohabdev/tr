@extends('admin.master')
@section('title')
	 - Regional Centre
@endsection

@section('mainPart')
	<main class="ttr-wrapper">
		<div class="container-fluid">
            @if(Session::has('message'))
                <div class="notification"><div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ Session::get('message') }}</div></div>
            @endif
			<div class="db-breadcrumb">
				<h4 class="breadcrumb-title">Dashboard</h4>
				<ul class="db-breadcrumb-list">
					<li><a href="{{ url('./admin') }}"><i class="fa fa-home"></i>Home</a></li>
					<li>Regional Centre</li>
				</ul>
			</div>
			<div class="row">
				<div class="col-md-12 col-lg-12 col-xl-12 col-sm-12 col-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Regional Centre Name</th>
                                <th>Chairman Name</th>
                                <th>Branch Code</th>
                                <th>Phone Number/Username</th>
                                <th>Password</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($regionals as $key => $regional)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $regional->regional_centre}}</td>
                                <td>{{ $regional->chairman_name}}</td>
                                <td>{{ $regional->branch_code}}</td>
                                <td>{{ $regional->mobile_number}}</td>
                                <td>{{ $regional->password}}</td>
                                <td>
                                	@if( $regional->status==0 )<span class="btn btn-sm btn-warning" style="color:green; font-weight: bolder;"> Pending </span>
                                    @elseif( $regional->status==1 )<span class="btn btn-sm btn-success"> Approved </span>
                                    @endif
                                <td>

                                	@if( $regional->status==0 )<a href="#" class="btn btn-sm btn-info" data-toggle="modal" data-target="#statusModal{{ $regional->id}}" class="mr-2" title="Approve!"> Approve </a>
                                    @endif
                                    <a href="#" title="Delete" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal{{ $regional->id}}" > Delete </i></a>
                                </td>
							        <!-- Status Modal -->
							    <div class="modal fade" id="statusModal{{ $regional->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							            <div class="modal-dialog" role="document">
							                <div class="modal-content">
							                <div class="modal-header">
							                    <h3 class="modal-title" id="exampleModalLabel" style="color:red;">Are you want to Approve this ???</h3>
							                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							                    <span aria-hidden="true">&times;</span>
							                    </button>
							                </div>
							                <form method="post" action="{{ url('./regional-status', $regional->id) }}">
							                    @csrf
							                    <div class="modal-body">
							                        <input type="hidden" name="id" value="{{ $regional->id}}" />
							                        <center><span class="h6 text-danger">Are you want to Approve this ???</span></center>
							                    </div>
							                    <div class="modal-footer">
							                        <button type="button" class="btn btn-success" data-dismiss="modal">No!</button>
							                        <button type="submit" class="btn btn-danger bg-danger">Yes! Approved</button>
							                    </div>
							                </form>
							                </div>
							            </div>
							        </div>
		        					<!-- Delete Modal -->
								    <div class="modal fade" id="deleteModal{{ $regional->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							            <div class="modal-dialog" role="document">
							                <div class="modal-content">
							                <div class="modal-header">
							                    <h5 class="modal-title" id="exampleModalLabel">Delete?</h5>
							                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							                    <span aria-hidden="true">&times;</span>
							                    </button>
							                </div>
							                <form method="post" action="{{ url('./regional-delete', $regional->id) }}">
							                    @csrf
							                    <div class="modal-body">
							                        <input type="hidden" name="id" value="{{ $regional->id}}" />
							                        <center><span class="h3 text-danger">Are you Sure ?</span></center>
							                    </div>
							                    <div class="modal-footer">
							                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							                        <button type="submit" class="btn btn-danger bg-danger">Delete</button>
							                    </div>
							                </form>
							                </div>
							            </div>
							        </div>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
    </main>
@endsection
