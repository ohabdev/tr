<!-- Left sidebar menu start -->
<div class="ttr-sidebar">
	<div class="ttr-sidebar-wrapper content-scroll">
		<!-- side menu logo start -->
		<div class="ttr-sidebar-logo">
			<a href="{{ url('./admin') }}"><img alt="" src="{{ asset('public') }}/assets/images/logo.png" width="122" height="27"></a>
			<div class="ttr-sidebar-toggle-button">
				<i class="ti-arrow-left"></i>
			</div>
		</div>
		<!-- side menu logo end -->
		<!-- sidebar menu start -->
		<nav class="ttr-sidebar-navi">
			<ul>
				<li>
					<a href="{{ url('./admin') }}" class="ttr-material-button">
						<span class="ttr-icon"><i class="ti-home"></i></span>
	                	<span class="ttr-label">Dashborad</span>
	                </a>
	            </li>
				<li>
					<a href="{{ url('./courses') }}" class="ttr-material-button">
						<span class="ttr-icon"><i class="ti-book"></i></span>
	                	<span class="ttr-label">Courses</span>
	                </a>
	            </li>
				<li>
					<a href="{{ url('./batches') }}" class="ttr-material-button">
						<span class="ttr-icon"><i class="ti-bookmark-alt"></i></span>
	                	<span class="ttr-label">Batch</span>
	                </a>
	            </li>
				<li>
					<a href="{{ url('./branch_view') }}" class="ttr-material-button">
						<span class="ttr-icon"><i class="ti-bookmark-alt"></i></span>
	                	<span class="ttr-label">Regional Centre</span>
	                </a>
	            </li>
				<li>
					<a href="#" class="ttr-material-button">
						<span class="ttr-icon"><i class="ti-user"></i></span>
	                	<span class="ttr-label">Result Publish</span>
	                	<span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
	                </a>
	                <ul>
	                	<li>
	                		<a href="{{ url('./result_publish') }}" class="ttr-material-button"><span class="ttr-icon"><i class="ti-bookmark-alt"></i></span><span class="ttr-label">Add New Result Publish</span></a>
	                	</li>
	                	<li>
	                		<a href="{{ url('./all_result') }}" class="ttr-material-button"><span class="ttr-icon"><i class="ti-bookmark-alt"></i></span><span class="ttr-label">All Result Publish</span></a>
	                	</li>
	                </ul>
	            </li>
	            <li class="ttr-seperate"></li>
			</ul>
			<!-- sidebar menu end -->
		</nav>
		<!-- sidebar menu end -->
	</div>
</div>
<!-- Left sidebar menu end -->
