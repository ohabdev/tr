@extends('admin.master')
@section('title')
     - Batch
@endsection

@section('mainPart')
    <main class="ttr-wrapper">
        <div class="container-fluid">
            @if(Session::has('message'))
                <div class="notification"><div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ Session::get('message') }}</div></div>
            @endif
            <div class="db-breadcrumb">
                <h4 class="breadcrumb-title">Dashboard</h4>
                <ul class="db-breadcrumb-list">
                    <li><a href="{{ url('./admin') }}"><i class="fa fa-home"></i>Home</a></li>
                    <li>Courses</li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl-12 col-sm-12 col-12">
                    <button class="btn bg-success mb-2" data-toggle="modal" data-target="#addBatch" style="color:white;">Add New Batch</button>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Duration</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($batches as $key => $batch)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $batch->name}}</td>
                                <td>@if( $batch->status==1 )<span class="badge badge-success">Active</span>
                                    @elseif( $batch->status==0 )<span class="badge badge-danger">Inactive</span>
                                    @endif
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#statusModal{{ $batch->id}}" class="mr-2" title="Change Status!"><i class="fa fa-refresh text-warning"></i></a>
                                    <a href="#" class="mr-2" title="Edit" data-toggle="modal" data-target="#editModal{{ $batch->id}}" ><i class="fa fa-edit text-info"></i></a>
                                    <a href="#" title="Delete" data-toggle="modal" data-target="#deleteModal{{ $batch->id}}" ><i class="fa fa-trash text-danger"></i></a>
                                </td>
                                <!-- Edit Modal -->
    <div class="modal fade" id="editModal{{ $batch->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Batch</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{ url('./batch-update') }}">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="CourseName">Batch Name</label>
                            <input type="hidden" class="form-control" name="id" id="id" value="{{ $batch->id}}">
                            <input type="text" class="form-control" name="name" id="CourseName" value="{{ $batch->name}}">
                        </div>
                        <div class="form-group">
                            <label for="Status">Status</label>
                            <select class="form-control" id="Status" name="status">
                                @if($batch->status==1)
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                @elseif($batch->status==0)
                                    <option value="0">Inactive</option>
                                    <option value="1">Active</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update changes</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- Status Modal -->
    <div class="modal fade" id="statusModal{{ $batch->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Change Status!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{ url('./batch-status') }}">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="id" value="{{ $batch->id}}" />
                        <center><span class="h3 text-danger">Are you Sure ?</span></center>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger bg-danger">Change</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- Delete Modal -->
    <div class="modal fade" id="deleteModal{{ $batch->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{ url('./batch-delete') }}">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="id" value="{{ $batch->id}}" />
                        <center><span class="h3 text-danger">Are you Sure ?</span></center>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger bg-danger">Delete</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
    <!-- Modal -->
    <div class="modal fade" id="addBatch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ url('./batch-create') }}">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="CourseName">Batch Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Enter Batch Name">
                    </div>
                    <div class="form-group">
                        <label for="Status">Status</label>
                        <select class="form-control" id="Status" name="status">
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection
