@extends('admin.master')
@section('title')
     - Result
@endsection

@section('mainPart')
    <main class="ttr-wrapper">
        <div class="container-fluid">
            @if(Session::has('message'))
                <div class="notification"><div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ Session::get('message') }}</div></div>
            @endif
            <div class="db-breadcrumb">
                <h4 class="breadcrumb-title">Online Result Apply Form</h4>
                <ul class="db-breadcrumb-list">
                    <li><a href="{{ url('./admin') }}"><i class="fa fa-home"></i>Home</a></li>
                    <li>Result Publish</li>
                </ul>
            </div>
            <form action="{{ url('./result_store')}}" method="POST"  enctype="multipart/form-data">
            	@csrf
	            <div class="card-header">
	            	<div class="row">
		            	<div class="col-md-6">
		            		<div class="row">
		            			<div class="col-md-3">
		            				<label class="pull-center">Regional Centre</label>
		            			</div>		
		            			<div class="col-md-7">
		            				<select class="form-control" name="branches_id" id="branches_id" required>
		            					<option>Select Regional Centre</option>
		            					@foreach($branchs as $branch)
		            					<option value="{{ $branch->id }}">{{ $branch->regional_centre }}</option>
		            					@endforeach
		            				</select>
		            			</div>		
		            		</div>
		            		<hr>
		            		<div class="row">
		            			<div class="col-md-3">
		            				<label class="pull-center">Branch Code :</label>
		            			</div>		
		            			<div class="col-md-7">
		            				<input type="text" readonly name="branch_code" class="form-control" id="branch_code_view" placeholder="Auto Load" required>
		            			</div>		
		            		</div>
		            		<hr>
		            		<div class="row">
		            			<div class="col-md-3">
		            				<label class="pull-center"> Student Name :</label>
		            			</div>		
		            			<div class="col-md-7">
		            				<input type="text" name="student_name" class="form-control" required>
		            			</div>		
		            		</div>
		            		<hr>
		            		<div class="row">
		            			<div class="col-md-3">
		            				<label class="pull-center"> Father's Name :</label>
		            			</div>		
		            			<div class="col-md-7">
		            				<input type="text" name="father_name" class="form-control" required>
		            			</div>		
		            		</div>
		            		<hr>
		            		<div class="row">
		            			<div class="col-md-3">
		            				<label class="pull-center"> Mother's Name :</label>
		            			</div>		
		            			<div class="col-md-7">
		            				<input type="text" name="mother_name" class="form-control" required>
		            			</div>		
		            		</div>
		            		<hr>
		            		<div class="row">
		            			<div class="col-md-3">
		            				<label class="pull-center"> Date of Birth :</label>
		            			</div>		
		            			<div class="col-md-7">
		            				<input type="date" name="date_of_birth" class="form-control" required>
		            			</div>		
		            		</div>
		            		<hr>
		            		<div class="row">
		            			<div class="col-md-3">
		            				<label class="pull-center"> Address :</label>
		            			</div>		
		            			<div class="col-md-7">
		            				<input type="text" name="address" class="form-control" placeholder="Vill:........Post:.......Ps:........Dist:......." required>
		            			</div>		
		            		</div>
		            	</div>
		            	<div class="col-md-6">
		            		<div class="row">
		            			<div class="col-md-3">
		            				<label class="pull-center">Phone Number :</label>
		            			</div>		
		            			<div class="col-md-7">
		            				<input type="text" name="mobile_number" class="form-control" required>
		            			</div>		
		            		</div>
		            		<hr>
		            		<div class="row">
		            			<div class="col-md-3">
		            				<label class="pull-center"> Course Name :</label>
		            			</div>		
		            			<div class="col-md-7">
		            				<select class="form-control" name="courses_id" id="courses_id" required>
		            					<option>Select Course</option>
		            					@foreach($courses as $course)
		            					<option value="{{ $course->id }}">{{ $course->name }}</option>
		            					@endforeach
		            				</select>
		            			</div>		
		            		</div>
		            		<hr>
		            		<div class="row">
		            			<div class="col-md-3">
		            				<label class="pull-center"> Course Code :</label>
		            			</div>		
		            			<div class="col-md-7">
		            				<input type="text" name="course_code" id="courses_code_view" readonly class="form-control" placeholder="Auto Load" required>
		            			</div>		
		            		</div>
		            		<hr>
		            		<div class="row">
		            			<div class="col-md-3">
		            				<label class="pull-center"> Registration No:</label>
		            			</div>		
		            			<div class="col-md-7">
		            				<input type="text" name="registration_number" class="form-control" required>
		            			</div>		
		            		</div>
		            		<hr>
		            		<div class="row">
		            			<div class="col-md-3">
		            				<label class="pull-center"> Serial Number:</label>
		            			</div>		
		            			<div class="col-md-7">
		            				<input type="text" name="serial_number" class="form-control" required>
		            			</div>		
		            		</div>
		            		<hr>
		            		<div class="row">
		            			<div class="col-md-3">
		            				<label class="pull-center"> Batch :</label>
		            			</div>		
		            			<div class="col-md-7">
		            				<select class="form-control" name="batches_id" required>
		            					<option>Select Batch</option>
		            					@foreach($batches as $batch)
		            					<option value="{{ $batch->id }}">{{ $batch->name }}</option>
		            					@endforeach
		            				</select>
		            			</div>		
		            		</div>
		            		<hr>
		            		<div class="row">
		            			<div class="col-md-3">
		            				<label class="pull-center"> Semester:</label>
		            			</div>		
		            			<div class="col-md-7">
		            				<input type="text" name="semester" class="form-control" placeholder="Final" required>
		            			</div>		
		            		</div>
		            	</div>
		            </div>
	            </div>
	            <hr>
	            <table class="table table-bordered table-gray text-center">
				  <thead>
				    <tr>
				      <th scope="col">#S.L</th>
				      <th scope="col">Course</th>
				      <th scope="col">Course Code</th>
				      <th scope="col">T. Marks</th>
				      <th scope="col">Get Marks</th>
				      <th scope="col">CGPA</th>
				      <th scope="col">Grade</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <th scope="row">1</th>
				      <td class="courses_name_view"></td>
				      <td class="courses_code_view"></td>
				      <td><input type="number" readonly step="any" name="total_marks" class="courses_marks_view" required></td>
				      <td><input type="number" step="any" name="get_marks" id="get_marks" required></td>
				      <td><input type="number" step="any" name="cgpa" required></td>
				      <td><input type="text" readonly step="any" name="grade" id="Grade" class="Grade" required></td>
				    </tr>
				  </tbody>
				</table>
	    		<hr>
	    		<div class="row">
	    			<div class="col-md-3">
	    				<label class="pull-center"> Upload Image( Image Max Size 5MB ) : </label>
	    			</div>		
	    			<div class="col-md-1">
	    				<i class="fa fa-user btn btn-success"></i>
	    			</div>		
	    			<div class="col-md-6">
	    				<input type="file" name="image" class="form-control" required>
	    			</div>		
	    			<div class="col-md-2" style="margin: 0 auto;">
	    				<button class="btn btn-lg btn-success" type="submit"> Submit </button>
	    			</div>		
	    		</div>
	    	</form>
    		<hr><br><br><br><br>
        </div>
    </main>
