@extends('admin.master')
@section('title')
	 - Dashboard
@endsection

@section('mainPart')
	<main class="ttr-wrapper">
		<div class="container-fluid">
			<div class="db-breadcrumb">
				<h4 class="breadcrumb-title">Dashboard</h4>
				<ul class="db-breadcrumb-list">
					<li><a href="{{ url('./admin') }}"><i class="fa fa-home"></i>Home</a></li>
					<li>Dashboard</li>
				</ul>
			</div>
			<!-- Card -->
			<div class="row">
				<div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
					<div class="widget-card widget-bg1">					 
						<div class="wc-item">
							<h4 class="wc-title">
								Regional Centre
							</h4>
							<span class="wc-des">
								<a href="{{ url('/branch_view') }}" style="color:white;"> See All Branch </a>
							</span>
							<span class="wc-stats">
								<span class="counter">{{ $totalBranch }}</span>
							</span>		
							<div class="progress wc-progress">
								<div class="progress-bar" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<hr>
							<span class="wc-progress-bx">
								<span class="wc-change" style="color:white !important;">
									<h4 class="wc-title"> Apporved: &nbsp;&nbsp;&nbsp;<span class="counter">{{ $branchapproved }}</span> </h4>
								</span>
								<span class="wc-number ml-auto" style="color:white !important;">
									<h4 class="wc-title"> Pending: &nbsp;&nbsp;&nbsp;<span class="counter">{{ $branchpending }}</span> </h4>
								</span>
							</span>
							<hr>
						</div>				      
					</div>
				</div>
				<div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
					<div class="widget-card widget-bg3">					 
						<div class="wc-item">
							<h4 class="wc-title">
								Total Course
							</h4>
							<span class="wc-des">
								<a href="#">See All Course</a>
							</span>
							<span class="wc-stats">
								<span class="counter">{{ $courses }}</span>
							</span>		
							<div class="progress wc-progress">
								<div class="progress-bar" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<span class="wc-progress-bx">
								<span class="wc-change">
									Active: 
								</span>
								<span class="wc-number ml-auto">
									Inactive: 
								</span>
							</span>
						</div>				      
					</div>
				</div>
				<div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
					<div class="widget-card widget-bg2">					 
						<div class="wc-item">
							<h4 class="wc-title">
								Total Branch
							</h4>
							<span class="wc-des">
								See All Branch
							</span>
							<span class="wc-stats">
								<span class="counter">{{ $branches }}</span>
							</span>		
							<div class="progress wc-progress">
								<div class="progress-bar" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<span class="wc-progress-bx">
								<span class="wc-change">
									Active: 
								</span>
								<span class="wc-number ml-auto">
									Inactive: 
								</span>
							</span>
						</div>				      
					</div>
				</div>
				<div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
					<div class="widget-card widget-bg4">					 
						<div class="wc-item">
							<h4 class="wc-title">
								Results
							</h4>
							<span class="wc-des">
								See All Result
							</span>
							<span class="wc-stats">
								<span class="counter">{{ $results }}</span>
							</span>		
							<div class="progress wc-progress">
								<div class="progress-bar" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<span class="wc-progress-bx">
								<span class="wc-change">
									Pending
								</span>
								<span class="wc-number ml-auto">
									Publish
								</span>
							</span>
						</div>				      
					</div>
				</div>
			</div>
			<!-- Card END -->
		</div>
	</main>
@endsection