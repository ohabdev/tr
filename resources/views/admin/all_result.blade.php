@extends('admin.master')
@section('title')
	 - All Result
@endsection

@section('mainPart')
	<main class="ttr-wrapper">
		<div class="container-fluid">
            @if(Session::has('message'))
                <div class="notification"><div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ Session::get('message') }}</div></div>
            @endif
			<div class="db-breadcrumb">
				<h4 class="breadcrumb-title">All Result List</h4>
				<ul class="db-breadcrumb-list">
					<li><a href="{{ url('./admin') }}"><i class="fa fa-home"></i>Home</a></li>
					<li>All Result</li>
				</ul>
			</div>
			<div class="row">
				<div class="col-md-12 col-lg-12 col-xl-12 col-sm-12 col-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Regional Centre</th>
                                <th>Student Name</th>
                                <th>Course Name</th>
                                <th>Batch</th>
                                <th>Registration</th>
                                <th>Serial Number</th>
                                <th>Phone number</th>
                                <th>Status</th>
                                <th>Approve ???</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="show_all_results">
                        	
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
    </main>
@endsection
