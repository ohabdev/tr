<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BranchRegistration;
use App\Course;
use App\Batch;
use App\ResultPublish;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $totalBranch = BranchRegistration::all();
        $branchApproved = BranchRegistration::where('status', 1)->get();
        $branchPending = BranchRegistration::where('status', 0)->get();
        $courses = Course::all();
        $branches = Batch::all();
        $results = ResultPublish::all();

        return view('admin.dashboard',[
            'totalBranch' => count($totalBranch),
            'courses' => count($courses),
            'branches' => count($branches),
            'results' => count($results),
            'branchapproved' => count($branchApproved),
            'branchpending' => count($branchPending)
        ]);
    }
}
