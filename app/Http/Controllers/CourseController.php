<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $courses = Course::all();
        return view('admin.courses', ['courses' => $courses]);
    }

    public function create(Request $request)
    {
        // dd($request->all());
        $course = new Course();
        $course->name = $request->name;
        $course->code = $request->code;
        $course->status = $request->status;
        $course->save();
        return redirect('/courses')->with('message','New Course Created Successfully!');
    }

    public function changeStatus(Request $request)
    {
        // dd($request->all());
        $course = Course::find($request->id);
        if ($course->status == 1) {
            $status = 0;
        }else{
            $status = 1;
        }
        $course->status = $status;
        $course->save();
        return redirect('/courses')->with('message','Course Status Changed Successfully!');
    }

    public function update(Request $request, Course $course)
    {
        // dd($request->all());
        $course = Course::find($request->id);
        $course->name = $request->name;
        $course->code = $request->code;
        $course->status = $request->status;
        $course->save();
        return redirect('/courses')->with('message','Course Updated Successfully!');
    }

    public function destroy(Request $request)
    {
        $course = Course::find($request->id);
        $course->delete();
        return redirect('/courses')->with('message','Course Deleted Successfully!');
    }
}
