<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BranchRegistration;
use App\User;

class BranchRegistrationController extends Controller
{
    public function branch_view()
    {
    	$regionals = BranchRegistration::all();
        return view('admin.regional_centre', ['regionals' => $regionals]);
    }
    public function index()
    {
    	return view('branch_registration');
    }
    public function store(Request $request)
    {
        $branches = new BranchRegistration();
        $branches->regional_centre = $request->regional_centre;
        $branches->branch_code = $request->branch_code;
        $branches->chairman_name = $request->chairman_name;
        $branches->address = $request->address;
        $branches->mobile_number = $request->mobile_number;
        $branches->password = $request->password;
        $branches->status = 0;
        $branches->save();

        $users = new User();
        $users->user_type = "user";
        $users->name = $request->chairman_name;
        $users->email = $request->mobile_number;
        $users->password = bcrypt($request->password);
        $users->save();
        return redirect()->back()->with('message','Congrats! Registration Successfully Completed.');
    }

    public function approve(Request $request)
    {
        $data = BranchRegistration::find($request->id);
        if ($data->status == 1) {
            $status = 0;
        }else{
            $status = 1;
        }
        $data->status = $status;
        $data->save();
        return redirect()->back()->with('message','Regional Successfully Approved');
    }

    public function destroy(Request $request)
    {
        $data = BranchRegistration::find($request->id);
        $data->delete();
        // $batch = User::find($request->id);
        // $batch->delete();
        return redirect()->back()->with('message','Regional Information Deleted Successfully!');
    }
}
