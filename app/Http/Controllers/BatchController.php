<?php

namespace App\Http\Controllers;

use App\Batch;
use Illuminate\Http\Request;

class BatchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $batches = Batch::all();
        return view('admin.batch', ['batches' => $batches]);
    }

    public function create(Request $request)
    {
        // dd($request->all());
        $batch = new Batch();
        $batch->name = $request->name;
        $batch->status = $request->status;
        $batch->save();
        return redirect('/batches')->with('message','New Batch Created Successfully!');
    }

    public function changeStatus(Request $request)
    {
        // dd($request->all());
        $batch = Batch::find($request->id);
        if ($batch->status == 1) {
            $status = 0;
        }else{
            $status = 1;
        }
        $batch->status = $status;
        $batch->save();
        return redirect('/batches')->with('message','Batch Status Changed Successfully!');
    }

    public function update(Request $request)
    {
        // dd($request->all());
        $batch = Batch::find($request->id);
        $batch->name = $request->name;
        $batch->status = $request->status;
        $batch->save();
        return redirect('/batches')->with('message','Batch Updated Successfully!');
    }

    public function destroy(Request $request)
    {
        $batch = Batch::find($request->id);
        $batch->delete();
        return redirect('/batches')->with('message','Batch Deleted Successfully!');
    }
}
