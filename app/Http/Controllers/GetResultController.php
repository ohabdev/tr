<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\ResultPublish;
use App\Course;

class GetResultController extends Controller
{
    public function index()
    {
    	$courses = Course::where('status',1)->get();
    	return view('get_result', ['courses' => $courses]);
    }
    // public function result_info( Request $request )
    // {
    // 	$courses_id = $request->courses_id; 
    // 	$serial_number = $request->serial_number; 
    // 	$registration_number = $request->registration_number;
    // 	$results = ResultPublish::where('courses_id', $courses_id)->where('serial_number', $serial_number)->where('registration_number', $registration_number)->get();
    // 	return view('result_info');
    // }
    public function result_info( Request $request )
    {
        // dd($request->all());
        $courses_id = $request->courses_id; 
        $serial_number = $request->serial_number; 
        $registration_number = $request->registration_number;
        $results = DB::table('result_publishes')
                               ->join('branch_registrations', 'result_publishes.branches_id', '=', 'branch_registrations.id')
                               ->join('batches', 'result_publishes.batches_id', '=', 'batches.id')
                               ->join('courses', 'result_publishes.courses_id', '=', 'courses.id')
                               ->select('result_publishes.*', 'branch_registrations.regional_centre as branch_name', 'batches.name as batch_name', 'courses.name as course_name')
                                ->where('courses_id', $courses_id)
                                ->where('serial_number', $serial_number)
                                ->where('registration_number', $registration_number)
                                ->where('result_publishes.status', '1')
                                ->get();
        // dd($results->all());
        return view('result_info', ['results' => $results]);
    }
}
