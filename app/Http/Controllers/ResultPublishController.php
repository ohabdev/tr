<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\BranchRegistration;
use App\ResultPublish;
use Carbon\Carbon;
use App\Course;
use App\Batch;

class ResultPublishController extends Controller
{
   public function index()
   {
   	$batches = Batch::all();
   	$courses = Course::all();
   	$branchs = BranchRegistration::all();
   	return view('admin.add_result_publish', [
   		'batches' => $batches, 
   		'courses' => $courses, 
   		'branchs' => $branchs
   	]);
   }
   
   public function branches_id(Request $request)
   {
      $data = BranchRegistration::find($request);
      return response()->json($data);
   }  
  public function courses_id(Request $request)
   {
      $data = Course::find($request);
      return response()->json($data);
   }
   public function result_store(Request $request)
   {
      if ($request->file('image')) 
      {
          $image = $request->file('image');
          if (isset($image))
          {
              $currentDate = Carbon::now()->toDateString();
              $imageName = 'student-'.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
              if (!Storage::disk('public')->exists('image'))
              {
                  Storage::disk('public')->makeDirectory('image');
              }
              $imagePath = Image::make($image)->resize(150,150)->stream();
              Storage::disk('public')->put('image/'.$imageName,$imagePath);
          } else {
              $imageName = "default_student_img.png";
          }
          $data = new ResultPublish();
          $data->branches_id = $request->branches_id;
          $data->branch_code = $request->branch_code;
          $data->student_name = $request->student_name;
          $data->father_name = $request->father_name;
          $data->mother_name = $request->mother_name;
          $data->date_of_birth = $request->date_of_birth;
          $data->address = $request->address;
          $data->mobile_number = $request->mobile_number;
          $data->courses_id = $request->courses_id;
          $data->course_code = $request->course_code;
          $data->registration_number = $request->registration_number;
          $data->serial_number = $request->serial_number;
          $data->batches_id = $request->batches_id;
          $data->semester = $request->semester;
          $data->total_marks = $request->total_marks;
          $data->get_marks = $request->get_marks;
          $data->cgpa = $request->cgpa;
          $data->grade = $request->grade;
          $data->image = $imageName;
          $data->status = 0;
          $data->save();

          if($data)
          {
              return redirect()->back()->with('message','Succcess!: Waiting for approval!');
          }
      }
   }

   public function all_result()
   {

      return view('admin.all_result');
   }
   public function all_result_load()
   {
      $results = DB::table('result_publishes')
               ->join('branch_registrations', 'result_publishes.branches_id', '=', 'branch_registrations.id')
               ->join('batches', 'result_publishes.batches_id', '=', 'batches.id')
               ->join('courses', 'result_publishes.courses_id', '=', 'courses.id')
               ->select('result_publishes.*', 'branch_registrations.regional_centre as branch_name', 'batches.name as batch_name', 'courses.name as course_name')
               ->get();
      return response()->json($results);
   }
}
