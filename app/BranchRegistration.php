<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchRegistration extends Model
{
    public function result_publish()
    {
    	return $this->belongsToMany('App\ResultPublish')->withTimesTamps();
    }
}
