<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultPublish extends Model
{
    public function branches()
    {
    	return $this->belongsToMany('App\BranchRegistration');
    }
    public function batches()
    {
    	return $this->belongsToMany('App\Batch');
    }
    public function courses()
    {
    	return $this->belongsToMany('App\Course');
    }
}
