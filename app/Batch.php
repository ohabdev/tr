<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    public function result_publish()
    {
    	return $this->belongsToMany('App\ResultPublish')->withTimesTamps();
    }
}
